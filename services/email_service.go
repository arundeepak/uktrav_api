package main

import (
  "fmt"
	"github.com/jasonlvhit/gocron"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
  "bytes"
  "mime/quotedprintable"
  "net/smtp"
  "strings"
)

var (
  id          int
	recipients  string
  subject     string
	body 				string
	sent        int
  count       int
)
const (
	SMTPServer = "smtp.gmail.com"
)


type Sender struct {
	User     string
	Password string
}

func NewSender(Username, Password string) Sender {

	return Sender{Username, Password}
}

func (sender Sender) SendMail(Dest []string, Subject, bodyMessage string) {

	msg := "From: " + sender.User + "\n" +
		"To: " + strings.Join(Dest, ",") + "\n" +
		"Subject: " + Subject + "\n" + bodyMessage

	err := smtp.SendMail(SMTPServer+":587",
		smtp.PlainAuth("", sender.User, sender.Password, SMTPServer),
		sender.User, Dest, []byte(msg))

	if err != nil {

		fmt.Printf("smtp error: %s", err)
		return
	}

	fmt.Println("Mail sent successfully!")
}

func (sender Sender) WriteEmail(dest []string, contentType, subject, bodyMessage string) string {

	header := make(map[string]string)
	header["From"] = sender.User

	receipient := ""

	for _, user := range dest {
		receipient = receipient + user
	}

	header["To"] = receipient
	header["Subject"] = subject
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = fmt.Sprintf("%s; charset=\"utf-8\"", contentType)
	header["Content-Transfer-Encoding"] = "quoted-printable"
	header["Content-Disposition"] = "inline"

	message := ""

	for key, value := range header {
		message += fmt.Sprintf("%s: %s\r\n", key, value)
	}

	var encodedMessage bytes.Buffer

	finalMessage := quotedprintable.NewWriter(&encodedMessage)
	finalMessage.Write([]byte(bodyMessage))
	finalMessage.Close()
	message += "\r\n" + encodedMessage.String()
	return message
}

func (sender *Sender) WriteHTMLEmail(dest []string, subject, bodyMessage string) string {
	return sender.WriteEmail(dest, "text/html", subject, bodyMessage)
}

func (sender *Sender) WritePlainEmail(dest []string, subject, bodyMessage string) string {
	return sender.WriteEmail(dest, "text/plain", subject, bodyMessage)
}


func email_init(subject, body, recipients string) {
  sender := NewSender("uttarakhandtraveler@gmail.com", "Traveller@25dec")

	//The receiver needs to be in slice as the receive supports multiple receiver

	Receiver := strings.Split(recipients,",")

	Subject := subject
	message := body
	bodyMessage := sender.WriteHTMLEmail(Receiver, Subject, message)

	sender.SendMail(Receiver, Subject, bodyMessage)
}


func emailQueue(db *sql.DB){
  /*_ = db.QueryRow("select count(*) from email_job where sent = ?", 0).Scan(&count)
  if count == 0 {
    gocron.Remove(t1)
  } */

  rows, _ := db.Query("select id,recipients,subject,body,sent from email_job where sent = ?",0)
  defer rows.Close()
  for rows.Next() {
    err := rows.Scan(&id,&recipients,&subject,&body,&sent)
    if err == nil {
      //fmt.Println(recipients)
      email_init(subject,body,recipients)
    }
    stmt, err := db.Prepare("UPDATE email_job set sent = 1 where id = ?")
    if err != nil {
      fmt.Println("Error Occurred on update")
    }
    stmt1,err := stmt.Exec(id)
    _ = stmt1
    if err == nil {
      fmt.Println("Updated successfully")
    }
  }
}

func main(){

        db, err := sql.Open("mysql", "uktraveller:uttarakhand@/uktrav?charset=utf8&parseTime=True")
  //db, err := sql.Open("mysql", "smartworks:smartworks@/smartworks?charset=utf8&parseTime=True")

  if err == nil {
    fmt.Println("DB Connected")
  }

  //gocron.Every(10).Second().Do(task,db)
  gocron.Every(1).Second().Do(emailQueue,db)
	<- gocron.Start()
}
