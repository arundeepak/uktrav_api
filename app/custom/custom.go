package custom

import (
	"fmt"
	"io"
	"math/rand"
	"mime/multipart"
	"os"
	"reflect"
	"strings"
	"time"
	"encoding/base64"
	"log"
    "image/gif"
    "image/jpeg"
    "image/png"
	"bytes"
	"strconv"
	"uktrav_api/app/models"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

//var Location *time.Location

func AuthenticateApi(db *gorm.DB, authtoken string) (models.User, string, bool) {
	var user models.User

	if authtoken == "" {
		return user, "Authentication Errxor! Auth Token is missing", false
	}

	if db.Where("authcode = ?", authtoken).First(&user).RecordNotFound() {
		return user, "Authentication Error! Token is invalid or does not belong to the user", false
	}

	return user, "", true
}

func CheckSession(db *gorm.DB, sessionKey string) (models.User, bool) {
	var user models.User
	var session models.Session
	Location, _ := time.LoadLocation("Asia/Kolkata")
	if db.Where("secret_key = ? AND expire_on > ?", sessionKey, time.Now().In(Location)).First(&session).RecordNotFound() {
		//if db.Where("secret_key = ?", sessionKey).First(&session).RecordNotFound() {
		return user, false
	}
	session.ExpireOn = (time.Now().In(Location)).AddDate(0, 0, 7)
	db.Save(&session)
	db.Model(&session).Related(&user)
	return user, true
}

const letterBytes = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func GenerateAuthString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func SendEmail(db *gorm.DB, subject, body, recipients string) bool {
	email := models.EmailJob{Recipients: recipients, Subject: subject, Body: body, Sent: 0}
	db.Create(&email)
	if db.NewRecord(email) {
		return false
	}
	return true
}

func UploadImage(db *gorm.DB,post models.Post, formData *multipart.Form) (bool) {
	for _, fileHeader := range formData.File["files"] {
		if len(fileHeader.Filename) == 0 {
			return true
		}
		file, _ := fileHeader.Open()
		fmt.Println(reflect.TypeOf(file))
		fileName := fileHeader.Filename
		//path := "/home/arundeepak/Desktop/go/src/"
		path := "/home/arundeepak92/webapps/uttarakhandtraveller_media_root/uploaded/"
		file_path := path + fileName
		url := "http://www.uttarakhandtraveller.com/media/uploaded/" + fileName
		f, err := os.OpenFile(file_path, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			fmt.Println(err)
		}
		defer f.Close()
		io.Copy(f, file)
		image := models.Image{Url: url, Status: 1}
		db.Create(&image)
		if db.NewRecord(image) {
			fmt.Println("Unable to create image")
		}
		//return image.Id, true
		postImage := models.PostImage{PostId: post.Id, ImageId: image.Id}
		db.Create(&postImage)
		if db.NewRecord(postImage) {
			fmt.Println("Unable to create image")
		}
	}
	return false
}

func SlugMaker(name string) string {
	name = strings.ToLower(name)
	name = strings.Replace(name, " ", "-", -1)
	name = strings.Replace(name, ",", "-", -1)
	name = strings.Replace(name, "#", "-", -1)
	name = strings.Replace(name, "%", "-", -1)
	name = strings.Replace(name, "&", "-", -1)
	name = strings.Replace(name, "*", "-", -1)
	name = strings.Replace(name, "{", "-", -1)
	name = strings.Replace(name, "}", "-", -1)
	name = strings.Replace(name, "/", "-", -1)
	name = strings.Replace(name, "|", "-", -1)
	name = strings.Replace(name, "?", "-", -1)
	return name
}

func ConvertDataToImage(db *gorm.DB, post models.Post, data string) bool {
	idx := strings.Index(data, ";base64,")
	result := true
	if idx < 0 {
		return false
        panic("InvalidImage")
    }
    ImageType := data[11:idx]
    log.Println(ImageType)

    unbased, err := base64.StdEncoding.DecodeString(data[idx+8:])
    if err != nil {
		return false
        panic("Cannot decode b64")
    }
	r := bytes.NewReader(unbased)
	//t := time.Now()
	//imageName := t.Format("20060102150405")
	imageName := "post_" + strconv.Itoa(post.Id)+"_"+GenerateAuthString(15)
	//path := "/home/arundeepak/Desktop/go/src/img/"
	path := "/home/arundeepak92/webapps/uttarakhandtraveller_media_root/uploaded/"
	switch ImageType {
		case "png":
			im, err := png.Decode(r)
			if err != nil {
				return false
				panic("Bad png")
			}
			imageName += ".png"
			imagePath := path + imageName
			f, err := os.OpenFile(imagePath, os.O_WRONLY|os.O_CREATE, 0777)
			if err != nil {
				return false
				panic("Cannot open file")
			}

			png.Encode(f, im)
		case "jpeg":
			im, err := jpeg.Decode(r)
			if err != nil {
				return false
				panic("Bad jpeg")
			}
			imageName += ".jpeg"
			imagePath := path + imageName
			f, err := os.OpenFile(imagePath , os.O_WRONLY|os.O_CREATE, 0777)
			if err != nil {
				return false
				panic("Cannot open file")
			}

			jpeg.Encode(f, im, nil)
		case "gif":
			im, err := gif.Decode(r)
			if err != nil {
				return false
				panic("Bad gif")
			}
			imageName += ".gif"
			imagePath := path + imageName
			f, err := os.OpenFile(imagePath, os.O_WRONLY|os.O_CREATE, 0777)
			if err != nil {
				return false
				panic("Cannot open file")
			}
			gif.Encode(f, im, nil)
		}
		url := "http://www.uttarakhandtraveller.com/media/uploaded/" + imageName
		image := models.Image{Url: url, Status: 1}
		db.Create(&image)
		if db.NewRecord(image) {
			fmt.Println("Unable to create image")
		}
		//return image.Id, true
		postImage := models.PostImage{PostId: post.Id, ImageId: image.Id}
		db.Create(&postImage)
		if db.NewRecord(postImage) {
			fmt.Println("Unable to create image")
		}
	return result
}

func get_notification_string(notif_type int, postTitle string) string {
	notification := ""
	switch ntype := notif_type; ntype {
	case 1:
		notification = "Your Post " + postTitle + " has been viewed by the admin."
	case 2:
		notification = "Your Post " + postTitle + " has been approved by the admin."
	case 3:
		notification = "Your Post " + postTitle + " has reached reached 100 views"
	case 4:
		notification = "Your Post " + postTitle + " has reached reached 500 views"
	case 5:
		notification = "Your Post " + postTitle + " has reached reached 1000 views"
	case 6:
		notification = "Your Post " + postTitle + " has reached reached 5000 views"
	default:
		fmt.Printf("Nothing found in the notif")
	}
	return notification
}


func GetNotifications(db *gorm.DB, blogger models.Blogger)([]string, bool){
	new_notifications := []string{}
	//all_notifications := []string{}
	postNotifications := []models.PostNotification{}
	db.Raw("select * from post_notification where status=1 and post_id in (select id from post where blogger_id = ?) order by cr_date desc", blogger.Id).Scan(&postNotifications)
	for _, postnotif := range postNotifications {
		var post models.Post
		db.First(&post, postnotif.PostId)
		new_notifications = append(new_notifications , get_notification_string(postnotif.NotifId,post.Title))
		postnotif.Status=0
		db.Save(&postnotif)
	}

	return new_notifications, true
}




func UploadImageNoDB(folderName string, formData *multipart.Form) (bool, string) {
	result := true
	url := ""
	for _, fileHeader := range formData.File["img"] {
		if len(fileHeader.Filename) == 0 {
			return false, url
		}
		file, _ := fileHeader.Open()
		fmt.Println(reflect.TypeOf(file))
		fileName := fileHeader.Filename
		//path := "/home/arundeepak/Desktop/" + folderName +"/"
		path := "/home/arundeepak92/webapps/uttarakhandtraveller_media_root/" + folderName +"/"
		file_path := path + fileName
		url = "http://www.uttarakhandtraveller.com/media/"+folderName+"/"+ fileName
		f, err := os.OpenFile(file_path, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			result = false
		}
		defer f.Close()
		io.Copy(f, file)
	}
	return result, url
}
