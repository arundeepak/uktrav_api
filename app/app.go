package app

import (
	"github.com/gorilla/sessions"
	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/middleware"
)

var Server *echo.Echo

// init echo web server
func Init() {
	Server = echo.New()
	Server.Debug = true
	// Middleware
	Server.Use(middleware.Logger())
	Server.Use(middleware.Recover())
	//Server.Use(session.MiddlewareWithConfig(session.Config{}))
	/*Server.Use(middleware.CSRFWithConfig(middleware.CSRFConfig{
		TokenLookup: "form:csrf",
	}))*/
	Server.Use(session.Middleware(sessions.NewCookieStore([]byte("secret"))))

	//CORS
	Server.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	/*Server.GET("/users", func(c echo.Context) error{
	  //dO something here
	    return c.String(http.StatusOK, "OK")
	  })*/
	//static file serviing
	//Server.Static("/static", "assets")
	Server.Static("/", "static")
}
