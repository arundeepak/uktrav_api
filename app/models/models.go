package models

import (
	"time"
)

type User struct {
	Id           int    `json:"id"`
	Fname        string `json:"fname"`
	Lname        string `json:"lname"`
	Email        string `json:"email"`
	Phone        string `json:"phone"`
	Authcode     string `json:"authcode"`
	PasswordSalt string
	Status       int
	CreatedDt    time.Time `json:"created_dt"`
}

type District struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Division string `json:"division"`
}

type Tag struct {
	Id     int    `gorm:"primary_key;"`
	Name   string `gorm:"not null;"`
	status int    `gorm:"not null;"`
}

type Place struct {
	Id          int       `gorm:"primary_key;"`
	District    District  `gorm:"foreignkey:DistrictId;association_foreignkey:Id"`
	DistrictId  int       `json:"dictrict_id"`
	Name        string    `gorm:"not null;"`
	Description string    `gorm:"not null;"`
	CrDate      time.Time `gorm:"not null;"`
	Destination string    `gorm:"not null;"`
	Status      int       `gorm:"not null;"`
}

type PLaceTag struct {
	Id      int   `gorm:"primary_key;"`
	Place   Place `gorm:"foreignkey:PlaceId;association_foreignkey:Id"`
	PlaceId int   `json:"place_id"`
	Tag     Tag   `gorm:"foreignkey:TagId;association_foreignkey:Id"`
	TagId   int   `json:"tag_id"`
}

type SubscribedEmail struct {
	Id     int       `json:"id"`
	Token  string    `json:"token"`
	Email  string    `json:"email"`
	CrDate time.Time `json:"cr_date"`
	Active int       `json:"active"`
}

type EmailJob struct {
	Id         int `gorm:"primary_key;"`
	Recipients string
	Subject    string
	Body       string
	Sent       int
}

type Blogger struct {
	Id       	int  `gorm:"primary_key;"`
	User     	User `gorm:"foreignkey:UserId;association_foreignkey:Id"`
	UserId   	int
	ShortBio 	string
	PassCode 	int
	Status   	int
	Description string
	Image 		string 
}

type Post struct {
	Id          int     `gorm:"primary_key;"`
	Blogger     Blogger `gorm:"foreignkey:BloggerId;association_foreignkey:Id"`
	BloggerId   int
	ImageUrl    string
	Title       string
	Description string
	CrDate      time.Time
	Status      int
	Slug        string
	View        int
	PostType    PostType `gorm:"foreignkey:TypeId;association_foreignkey:Id"`
	TypeId   	int	
}

type PhotoBank struct {
	Id       int       `json:"id"`
	Url      string    `json:"url"`
	Location string    `json:"location"`
	Username string    `json:"username"`
	CrDate   time.Time `json:"cr_date"`
}

type Role struct {
	Id   int    `json:"id"`
	Type string `json:"type"`
	Name string `json:"name"`
}

type Session struct {
	Id        int       `json:"id"`
	User      User      `gorm:"foreignkey:UserId;association_foreignkey:Id"`
	UserId    int       `json:"user_id"`
	Role      Role      `gorm:"foreignkey:RoleId;association_foreignkey:Id"`
	RoleId    int       `json:"role_id"`
	SecretKey string    `json:"secret_key"`
	CreatedOn time.Time `json:"created_on"`
	ExpireOn  time.Time `json:"expire_on"`
}

type Image struct {
	Id     int    `json:"id"`
	Url    string `json:"url"`
	Status int    `json:"status"`
}

type PostImage struct {
	Id      int   `json:"id"`
	Post    Post  `gorm:"foreignkey:PostId;association_foreignkey:Id"`
	PostId  int   `json:"post_id"`
	Image   Image `gorm:"foreignkey:ImageId;association_foreignkey:Id"`
	ImageId int   `json:"image_id"`
}

type PostTag struct {
	Id      int   `gorm:"primary_key;"`
	Post   	Post  `gorm:"foreignkey:PostId;association_foreignkey:Id"`
	PostId 	int   `json:"post_id"`
	Tag     Tag   `gorm:"foreignkey:TagId;association_foreignkey:Id"`
	TagId   int   `json:"tag_id"`
}

type Superuser struct {
	Id      int     `gorm:"primary_key;"`
	User   	User    `gorm:"foreignkey:UserId;association_foreignkey:Id"`
	UserId 	int     `json:"post_id"`
	passkey string  `gorm:"foreignkey:TagId;association_foreignkey:Id"`
	TagId   int     `json:"tag_id"`
}

type PostType struct {
	Id      int     `gorm:"primary_key;"`
	Type    string 	`json:"type"`
}

type PostNotificationType struct {
	Id      int     `gorm:"primary_key;"`
	Ntype    string 	`json:"ntype"`
}

type PostNotification struct {
	Id      				int     `				gorm:"primary_key;"`
	PostNotificationType   	PostNotificationType    `gorm:"foreignkey:NotifId;association_foreignkey:Id"`
	NotifId 				int     				`json:"notif_id"`
	Post   					Post    				`gorm:"foreignkey:PostId;association_foreignkey:Id"`
	PostId 					int     				`json:"post_id"`
	Status					int 					`json: "status"`
	CrDate 					time.Time 				`json:"cr_date"`	 				
}