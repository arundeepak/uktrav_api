alter table user modify column password_salt varchar(60) NOT NULL;
create table role(
	id INT NOT NULL AUTO_INCREMENT,
    type VARCHAR(300) NOT NULL,
    name VARCHAR(30) NOT NULL,
    PRIMARY KEY (id)
);

create table session(
	id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    role_id INT NOT NULL,
    secret_key VARCHAR(300) NOT NULL,
    created_on datetime NOT NULL,
    expire_on datetime default NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (role_id) REFERENCES role(id)
);

INSERT INTO role(type,name) VALUES("A user in Uttarakhand Traveller having user access","user");
INSERT INTO role(type,name) VALUES("A blogger in Uttarakhand Traveller having blog access","blogger");
alter table user modify column phone varchar(15) DEFAULT NULL;
alter table user modify column authcode varchar(60) DEFAULT NULL;
alter table user modify column email varchar(50) unique;

create table post_image(
	id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    image_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (post_id) REFERENCES post(id),
    FOREIGN KEY (image_id) REFERENCES image(id)
);

alter table post add column view int default 0;

create table post_messages(
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    recieve text NOT NULL,
    cr_date datetime NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (post_id) REFERENCES post(id)
);


create table superuser(
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    passkey varchar(30) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES user(id)
);

create table post_type(
    id INT NOT NULL AUTO_INCREMENT,
    type varchar(10) not null,
    PRIMARY KEY (id)
);

INSERT INTO post_type(type) VALUES("Travel");
INSERT INTO post_type(type) VALUES("Culture");
INSERT INTO post_type(type) VALUES("Food");
INSERT INTO post_type(type) VALUES("Others");

ALTER TABLE blogger add column description text default null;
ALTER TABLE blogger add column image varchar(300) default null;



create table post_notification_type(
    id INT NOT NULL AUTO_INCREMENT,
    ntype VARCHAR(100) NOT NULL,    
    PRIMARY KEY (id)
);

insert into post_notification_type(ntype) VALUES("Post has been viewed");
insert into post_notification_type(ntype) VALUES("Post has been approved");
insert into post_notification_type(ntype) VALUES("Post has reached 100 views");
insert into post_notification_type(ntype) VALUES("Post has reached 500 views");
insert into post_notification_type(ntype) VALUES("Post has reached 1000 views");
insert into post_notification_type(ntype) VALUES("Post has reached 5000 views");

create table post_notification(
    id INT NOT NULL AUTO_INCREMENT,
    notif_id INT NOT NULL,
    post_id INT NOT NULL,
    status TINYINT NOT NULL DEFAULT 1,
    cr_date datetime NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (notif_id) REFERENCES post_notification_type(id),
    FOREIGN KEY (post_id) REFERENCES post(id)
);

insert into post_notification(notif_id,post_id) VALUES(3,8);
insert into post_notification(notif_id,post_id) VALUES(3,9);
insert into post_notification(notif_id,post_id) VALUES(5,10);