package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
	"uktrav_api/app"
	"uktrav_api/app/custom"
	"uktrav_api/app/models"

	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo-contrib/session"
	"golang.org/x/crypto/bcrypt"
)

//Templates
type Template struct {
	templates *template.Template
}

const (
	//PROJECT_URL = "http://127.0.0.1:1234/api/v1/"
	//GO_UKTRAV   = "http://127.0.0.1:1234/"
	GO_UKTRAV   = "http://go.uttarakhandtraveller.com/"
	//GO_UKTRAV   = "http://staging.uttarakhandtraveller.com/"
	PROJECT_URL = "http://go.uttarakhandtraveller.com/api/v1/"
	//PROJECT_URL = "http://staging.uttarakhandtraveller.com/api/v1/"
)

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

var IndexPageRenderer *Template

//Custom Mapper used in the Handlers.
type H map[string]interface{}

//Custom Response required in each handler/API
type Response struct {
	ErrorMessage string
	Result       bool
}

var Location *time.Location

func Init() {
	var DB *gorm.DB
	//DB, err := gorm.Open("mysql", "smartworks:smartworks@/uktrav?charset=utf8&parseTime=True")
	DB, err := gorm.Open("mysql", "uktraveller:uttarakhand@/uktrav?charset=utf8&parseTime=True")
	//DB, err := gorm.Open("mysql", "smartworks:smartworks@tcp(smartworks-staging-db.cba5wuehk2rr.ap-southeast-1.rds.amazonaws.com)/visitor_management_system?charset=utf8&parseTime=True")
	if err != nil {
		fmt.Println("DB not Connected")
	}
	DB.SingularTable(true)
	Location, _ = time.LoadLocation("Asia/Kolkata")

	//static files renderer
	IndexPageRenderer = &Template{
		templates: template.Must(template.ParseGlob("templates/*.html")),
	}

	// Custom error hander
	//pp.Server.HTTPErrorHandler = customHTTPErrorHandler

	// ###################################### ROUTES ###########################################
	app.Server.GET("/", get_index_page(DB))
	app.Server.GET("/register", register(DB))
	app.Server.POST("/register_attempt", register_attempt(DB))
	app.Server.GET("/upload", upload_photos_noplace(DB))
	app.Server.GET("/upload/:place", upload_photos(DB))
	app.Server.POST("/uploaded", uploaded(DB))
	app.Server.GET("/show_images", show_photos(DB))
	app.Server.GET("/reset_password", reset_password(DB))
	//###################################### BLOG ROUTES ###################################
	blogRoute := app.Server.Group("/blog")
	blogRoute.GET("/", dashboard(DB))
	blogRoute.GET("/edit_article/:id", edit_article(DB))
	blogRoute.GET("/add_article", add_article(DB))
	blogRoute.GET("/articlesubmitted", articleSubmittedPage(DB))
	blogRoute.POST("/submit", article_submit(DB))
	blogRoute.GET("/login", blogger_login(DB))
	blogRoute.POST("/login_attempt", blogger_login_success(DB))
	blogRoute.GET("/logout", blogger_logout(DB))
	blogRoute.GET("/profile/:id", blogger_profile(DB))
	blogRoute.POST("/profile_submit/:id", profile_submit(DB))
	//app.Server.GET("/blogger", get_index_page(DB))
	//articlesubmitted
	//############## REST API ROUTES #######################################################
	v1 := app.Server.Group("/api/v1")
	v1.GET("/test", helloWorld(DB))
	v1.GET("/places/:id", getPlace(DB))
	v1.GET("/sub_email/:email", subEmail(DB))
	v1.GET("/confirm/:token", subConfirm(DB))
	v1.GET("/confirm_registration/:token", registrationConfirm(DB))

	v1.POST("/subscribe", subEmailFromWebHook(DB))
	v1.POST("/article_submit", articleSubmit(DB))
	v1.GET("/publish_article/:id", pubArticle(DB))
	//v1.GET("/approve_article/:id", approveArticle(DB))
        v1.POST("/contact_submit", contactSubmit(DB))
}

// ################################ REST APIs ###################################################

func helloWorld(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.JSON(http.StatusOK, H{"Hello": "User, your app is working"})
	}
}

func contactSubmit(db *gorm.DB) echo.HandlerFunc {

	return func(c echo.Context) error {
		data := H{}
		response := Response{"", true}
		jsonReq := H{}
		err := json.NewDecoder(c.Request().Body).Decode(&jsonReq)
		if err != nil {
			data["result"] = false
			data["error_message"] = "Invalid Json Request"
			return c.JSON(http.StatusOK, data)
		}
		name := jsonReq["name"].(string)
		if len(name) == 0 {
			fmt.Println("No name")
		}
		email := jsonReq["email"].(string)
		 if len(email) == 0 {
                        fmt.Println("No email")
                }
		subject := jsonReq["subject"].(string)
		 if len(subject) == 0 {
                        fmt.Println("no subject")
			response.Result=false
                }
		body := jsonReq["body"].(string)
		 if len(body) == 0 {
                        fmt.Println("No name")
			response.Result=false
                }

		body = body + "-- email by "+ name +" ( "+ email +" ) "
		if !custom.SendEmail(db, subject, body, "arundeepak92@gmail.com") {
			data["result"] = false
			data["error_message"] = "Unable to send email"
			return c.JSON(http.StatusOK, data)
		}

		data["result"] = response.Result
		data["error_message"] = response.ErrorMessage
		return c.JSON(http.StatusOK, data)
	}

}

//approveArticle
/*func approveArticle(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
			id, _ := strconv.Atoi(c.Param("id"))
			var post models.Post
			if db.First(&post, id).RecordNotFound() {
				return c.Render(http.StatusNotFound, "not_found.html", H{})
			}

			type Image struct {
				Id   int    `json:"id"`
				Url  string `json:"url"`
			}
			images := []Image{}
			db.Raw("SELECT i.id as id, i.url as name FROM image as i,post_image as pi WHERE pi.image_id=i.id and pi.post_id = ?", post.Id).Scan(&images)
		
			tags := []models.Tag{}
			db.Find(&tags)
			messages := H{
				"title":       post.Title,
				"slug" :       post.Slug,
				"description": post.Description,
				"post_id":     post.Id,
				"images":      images,
				"tags":        tags,
			}
		return c.JSON(http.StatusOK, messages)
	}
}*/


func getPlace(db *gorm.DB) echo.HandlerFunc {

	return func(c echo.Context) error {
		data := H{}
		response := Response{"", true}
		user, message, ok := custom.AuthenticateApi(db, c.Request().Header.Get("authtoken"))
		if !ok {
			data["result"] = false
			data["error_message"] = message
			return c.JSON(http.StatusUnauthorized, data)
		}
		_ = user
		id, _ := strconv.Atoi(c.Param("id"))
		var place models.Place
		if db.First(&place, id).RecordNotFound() {
			data["result"] = false
			data["error_message"] = "No place found"
			return c.JSON(http.StatusOK, data)
		}

		type Tag struct {
			Id   int    `json:"id"`
			Name string `json:"name"`
		}
		tags := []Tag{}
		db.Raw("SELECT t.id as id, t.name as name FROM tag as t,place_tag as pt WHERE pt.tag_id=t.id and pt.place_id = ?", place.Id).Scan(&tags)

		var district models.District
		db.Model(&place).Related(&district)
		place_details := H{
			"name":        place.Name,
			"district":    district,
			"description": place.Description[:300],
			"cr_date":     place.CrDate,
			"destination": place.Destination,
			"status":      place.Status,
			"tags":        tags,
		}
		data["place_details"] = place_details

		data["result"] = response.Result
		data["error_message"] = response.ErrorMessage
		return c.JSON(http.StatusOK, data)
	}

}

func subEmail(db *gorm.DB) echo.HandlerFunc {

	return func(c echo.Context) error {
		data := H{}
		response := Response{"", true}
		/* user,message, ok := custom.AuthenticateApi(db, c.Request().Header.Get("authtoken"))
		if !ok {
			data["result"] = false
			data["error_message"] = message
			return c.JSON(http.StatusUnauthorized, data)
		}
		_ = user */
		q_email := c.Param("email")
		email := models.SubscribedEmail{Email: q_email, Token: custom.GenerateAuthString(50), CrDate: time.Now().In(Location), Active: 0}
		db.Create(&email)
		if db.NewRecord(email) {
			data["result"] = false
			data["error_message"] = "Unable to add email"
			return c.JSON(http.StatusOK, data)
		}

		subject := "Welcome to Uttarakhand Traveller News Feeds"
		body := "Please on click this link to subscribe our Newsletters \n" + PROJECT_URL + "confirm/" + email.Token
		if !custom.SendEmail(db, subject, body, email.Email) {
			data["result"] = false
			data["error_message"] = "Unable to send email"
			return c.JSON(http.StatusOK, data)
		}

		data["result"] = response.Result
		data["error_message"] = response.ErrorMessage
		return c.JSON(http.StatusOK, data)
	}

}

func subEmailFromWebHook(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		data := H{}
		response := Response{"", true}

		jsonReq := H{}
		err := json.NewDecoder(c.Request().Body).Decode(&jsonReq)
		if err != nil {
			data["result"] = false
			data["error_message"] = "Invalid JSON request"
			return c.JSON(http.StatusOK, data)
		}
		resp := jsonReq["response"]

		var qEmail string
		switch x := resp.(type) {
		case []interface{}:
			for _, element := range x {
				k := element.(map[string]interface{})
				field := k["field"]
				typ := field.(map[string]interface{})
				if typ["type"] == "email" {
					qEmail = k["value"].(string)
				}
			}
		default:
			fmt.Printf("I don't know how to handle %T\n", resp)
		}

		email := models.SubscribedEmail{Email: qEmail, Token: custom.GenerateAuthString(50), CrDate: time.Now().In(Location), Active: 0}
		db.Create(&email)
		if db.NewRecord(email) {
			data["result"] = false
			data["error_message"] = "Unable to add email"
			return c.JSON(http.StatusOK, data)
		}

		subject := "Welcome to Uttarakhand Traveller News Feeds"
		body := "Please on click this link to subscribe our Newsletters \n" + PROJECT_URL + "confirm/" + email.Token
		if !custom.SendEmail(db, subject, body, email.Email) {
			data["result"] = false
			data["error_message"] = "Unable to send email"
			return c.JSON(http.StatusOK, data)
		}

		data["result"] = response.Result
		data["error_message"] = response.ErrorMessage
		return c.JSON(http.StatusOK, data)
	}
}

func pubArticle(db *gorm.DB) echo.HandlerFunc {

	return func(c echo.Context) error {
		data := H{}
		response := Response{"", true}
		user, message, ok := custom.AuthenticateApi(db, c.Request().Header.Get("authtoken"))
		if !ok {
			data["result"] = false
			data["error_message"] = message
			return c.JSON(http.StatusUnauthorized, data)
		}
		_ = user
		id, _ := strconv.Atoi(c.Param("id"))
		var post models.Post
		if db.First(&post, id).RecordNotFound() {
			data["result"] = false
			data["error_message"] = "No place found"
			return c.JSON(http.StatusOK, data)
		}

		post_details := H{
			"title":       post.Title,
			"description": post.Description[:500],
			"image_url":   "http://www.uttarakhandtraveller.com/media/" + post.ImageUrl,
			"url":         "http://www.uttarakhandtraveller.com/posts/" + strconv.Itoa(post.Id),
		}

		subject := post.Title + "|Uttarakhand Traveller"
		t, err := template.ParseFiles("templates/publish.html")
		if err != nil {
			data["result"] = false
			data["error_message"] = "Error Parsing Template"
			return c.JSON(http.StatusOK, data)
		}
		buf := new(bytes.Buffer)
		if err = t.Execute(buf, post_details); err != nil {
			data["result"] = false
			data["error_message"] = "Error Executing Template"
			return c.JSON(http.StatusOK, data)
		}
		body := buf.String()
		emailJob := models.EmailJob{Recipients: "arundeepak92@gmail.com", Subject: subject, Body: body}
		db.Create(&emailJob)
		if db.NewRecord(emailJob) {
			fmt.Println("Unable to send email")
		}
		data["result"] = response.Result
		data["error_message"] = response.ErrorMessage
		return c.JSON(http.StatusOK, data)
	}

}


func articleSubmit(db *gorm.DB) echo.HandlerFunc {

	return func(c echo.Context) error {
		data := H{}
		response := Response{"", true}
		user, ok := custom.CheckSession(db, c.Request().Header.Get("authtoken"))
		if !ok {
			data["result"] = false
			data["error_message"] = "Invalid Token"
			return c.JSON(http.StatusUnauthorized, data)
		}
		fmt.Println(user.Fname)
		jsonReq := H{}
		err := json.NewDecoder(c.Request().Body).Decode(&jsonReq)
		if err != nil {
			data["result"] = false
			data["error_message"] = "Invalid Json Request"
			return c.JSON(http.StatusOK, data)
		}
		title := jsonReq["title"].(string)
		if len(title) == 0 {
			data["result"] = false
			data["error_message"] = "Title is missing"
			return c.JSON(http.StatusOK, data)
		}
		description := jsonReq["description"].(string)
		if len(description) == 0 {
			data["result"] = false
			data["error_message"] = "Description is missing"
			return c.JSON(http.StatusOK, data)
		}

		var blogger models.Blogger
		db.Where("user_id=?", user.Id).First(&blogger)
		slug := custom.SlugMaker(title)	
		type_id := 4
		post := models.Post{BloggerId: blogger.Id, Title: title, Description: description, CrDate: time.Now().In(Location), Status: 0, Slug: slug, TypeId: type_id}
		db.Create(&post)
		if db.NewRecord(post) {
			fmt.Println("Unable to create image")
		} 
		
		img := jsonReq["img"]
		if img != nil{
			switch x := img.(type) {
			case []interface{}:
				for _, img_data := range x {
					//fmt.Println(img_data)
					result := custom.ConvertDataToImage(db, post, img_data.(string))
					if result{
						fmt.Println(" image success")
					}else{
						fmt.Println("Error occured while reading")
					}
				}
			} 
		}
		post_data := H{"title": title, "description": description}
		data["post"] = post_data
		data["result"] = response.Result
		data["error_message"] = response.ErrorMessage
		return c.JSON(http.StatusOK, data)
	}

}

// ######################################################################################################################

// ############################################ NON REST HANDLERS #######################################################




//registrationConfirm

func registrationConfirm(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		token := c.Param("token")
		var user models.User
		if db.Where("authcode = ?", token).First(&user).RecordNotFound() {
			return c.Render(http.StatusOK, "404.html", H{})
		}
		if user.Status == 1 {
			return c.Render(http.StatusOK, "404.html", H{})
		}
		user.Status = 1
		db.Save(&user)
		var blogger models.Blogger
		if db.Where("user_id = ?", user.Id).First(&blogger).RecordNotFound() {
			return c.Render(http.StatusOK, "404.html", H{})
		}
		blogger.Status = 1
		db.Save(&blogger)
		messages := H{
			"message": "Registration",
		}
		return c.Render(http.StatusOK, "sub_success.html", messages)
	}
}

func subConfirm(db *gorm.DB) echo.HandlerFunc {

	return func(c echo.Context) error {
		token := c.Param("token")
		var email models.SubscribedEmail
		if db.Where("token = ?", token).First(&email).RecordNotFound() {
			return c.Render(http.StatusOK, "404.html", H{})
		}
		if email.Active == 1 {
			return c.Render(http.StatusOK, "404.html", H{})
		}
		email.Active = 1
		db.Save(&email)
		messages := H{
			"message": "Subscription",
		}
		return c.Render(http.StatusOK, "sub_success.html", messages)
	}
}

func get_index_page(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.Render(http.StatusOK, "index.html", H{})
	}
}

func register(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.Render(http.StatusOK, "register.html", H{})
	}
}

//register_attempt

func register_attempt(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		fname := c.FormValue("firstname")
		lname := c.FormValue("lastname")
		email := c.FormValue("email")
		password := c.FormValue("password")
		passwordagain := c.FormValue("passwordagain")
		if password != passwordagain {
			messages := H{
				"message": "Passwords do not match",
				"fname":   fname,
				"lname":   lname,
				"email":   email,
			}
			return c.Render(http.StatusOK, "register.html", messages)
		}
		cost, _ := bcrypt.Cost([]byte(password))
		hashed, _ := bcrypt.GenerateFromPassword([]byte(password), cost)
		passwordhashed := string(hashed)
		token := custom.GenerateAuthString(50)
		user := models.User{Fname: fname, Lname: lname, Email: email, PasswordSalt: passwordhashed, Authcode: token, Status: 0, CreatedDt: time.Now().In(Location)}
		db.Create(&user)
		if db.NewRecord(user) {
			messages := H{
				"message": "Email already exists",
				"fname":   fname,
				"lname":   lname,
				"email":   email,
			}
			return c.Render(http.StatusOK, "register.html", messages)
		}

		blogger := models.Blogger{UserId: user.Id, Status: 0}
		db.Create(&blogger)
		if db.NewRecord(blogger) {
			fmt.Println("blogger not created")
		}

		subject := "Welcome to Uttarakhand Traveller Blog"
		body := "Please on click this link to confirm your registration with us \n" + PROJECT_URL + "confirm_registration/" + user.Authcode
		if !custom.SendEmail(db, subject, body, user.Email) {
			fmt.Println("Unable to send email")
		}
		messages := H{
			"register_url": GO_UKTRAV + "register",
			"message":      "Registration was successful, please check email and click on confirmation link to login",
		}
		return c.Render(http.StatusOK, "login.html", messages)
	}
}

//~, #, %, & , *, {, }, \, :, <, >, ?, /, |, “

func replaceSpecial(location string) string {
	location = strings.Replace(location, " ", "_", -1)
	location = strings.Replace(location, "~", "_", -1)
	location = strings.Replace(location, "#", "_", -1)
	location = strings.Replace(location, "%", "_", -1)
	location = strings.Replace(location, "&", "_", -1)
	location = strings.Replace(location, "*", "_", -1)
	location = strings.Replace(location, "{", "_", -1)
	location = strings.Replace(location, "}", "_", -1)
	location = strings.Replace(location, "/", "_", -1)
	location = strings.Replace(location, "|", "_", -1)
	location = strings.Replace(location, "?", "_", -1)
	return location
}

//upload_photos

func upload_photos(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		place := c.Param("place")
		place_details := H{
			"location": place,
		}
		return c.Render(http.StatusOK, "upload.html", place_details)
	}
}

//upload_photos_noplace

func upload_photos_noplace(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		//place := c.Param("place")
		place_details := H{
			"location": "",
		}
		return c.Render(http.StatusOK, "upload.html", place_details)
	}
}

func uploaded(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		fmt.Println(c.MultipartForm())
		formData, _ := c.MultipartForm()
		//here are the changes
		location := c.FormValue("location")
		username := c.FormValue("username")
		_ = username
		for _, fileHeader := range formData.File["files"] {

			file, _ := fileHeader.Open()
			fmt.Println(reflect.TypeOf(file))
			fmt.Println(file)
                        fileName := fileHeader.Filename
			//fileName := custom.GenerateAuthString(15)
			//path := "/home/arundeepak/Desktop/go/src/"
			path := "/home/arundeepak92/webapps/uttarakhandtraveller_media_root/uploaded/"
			//file_name := replaceSpecial(location) + "_" + fileName + ".jpg"
			file_path := path + fileName
			url := "http://www.uttarakhandtraveller.com/media/uploaded/" + fileName
			f, err := os.OpenFile(file_path, os.O_WRONLY|os.O_CREATE, 0666)
			if err != nil {
				fmt.Println(err)
			}
			defer f.Close()
			io.Copy(f, file)

			photo := models.PhotoBank{Url: url, Username: username, Location: location, CrDate: time.Now().In(Location)}
			db.Create(&photo)
			if db.NewRecord(photo) {
				fmt.Println("Unable to send email")
			}
		}
		return c.Render(http.StatusOK, "upload_success.html", H{})
	}
}

//show_photos

func show_photos(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {

		photos := []models.PhotoBank{}
		db.Find(&photos)
		return c.Render(http.StatusOK, "show_images.html", photos)
	}
}

//blogger_login
func blogger_login(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		sess, _ := session.Get("session", c)
		if sess.Values["s_key"] != nil {
			_, ok := custom.CheckSession(db, sess.Values["s_key"].(string))
			if ok {
				sess, _ := session.Get("session", c)
				sess.Options.MaxAge = 86400 * 7
				sess.Save(c.Request(), c.Response())
				c.Redirect(http.StatusFound, "/blog/")
			}
		}
		login_messages := H{
			"message":      "",
			"project_url":  GO_UKTRAV + "reset_password",
			"register_url": GO_UKTRAV + "register",
		}
		return c.Render(http.StatusOK, "login.html", login_messages)
	}
}

func blogger_login_success(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		valid := true
		email := c.FormValue("email")
		password := c.FormValue("password")
		var user models.User

		if !db.Where("email= ? and status= ?", email, 1).First(&user).RecordNotFound() {
			err := bcrypt.CompareHashAndPassword([]byte(user.PasswordSalt), []byte(password))
			if err != nil {
				valid = false
			} else {
				var blogger models.Blogger
				if db.Where("user_id= ?", user.Id).First(&blogger).RecordNotFound() {
					valid = false
				}
			}
		} else {
			valid = false
		}

		if valid {
			sess, _ := session.Get("session", c)
			sess.Options = &sessions.Options{
				Path:     "/",
				MaxAge:   86400 * 7,
				HttpOnly: true,
			}
			token := custom.GenerateAuthString(50)
			sess.Values["s_key"] = token
			sess.Save(c.Request(), c.Response())
			new_session := models.Session{UserId: user.Id, RoleId: 2, SecretKey: token, CreatedOn: time.Now().In(Location), ExpireOn: (time.Now().In(Location)).AddDate(0, 0, 7)}
			db.Create(&new_session)
			if db.NewRecord(new_session) {
				fmt.Println("Unable to send email")
			}
			c.Redirect(http.StatusFound, "/blog/")

		}

		login_messages := H{
			"message":      "Invalid Email/Password",
			"project_url":  GO_UKTRAV + "reset_password",
			"register_url": GO_UKTRAV + "register",
		}
		return c.Render(http.StatusOK, "login.html", login_messages)
	}
}

//dashboard

func dashboard(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		sess, _ := session.Get("session", c)
		if sess.Values["s_key"] == nil {
			return sessionExpired(c)
		}
		user, ok := custom.CheckSession(db, sess.Values["s_key"].(string))
		has_posts := true
		posts := []models.Post{}
		if ok {
			/*sess, _ := session.Get("session", c)
			sess.Options.MaxAge = 86400 * 7
			sess.Save(c.Request(), c.Response())
			*/
			var superuser models.Superuser
			var blogger models.Blogger
			if db.Where("user_id= ?", user.Id).First(&superuser).RecordNotFound(){
				if db.Where("user_id = ?", user.Id).Find(&blogger).RecordNotFound() {
					fmt.Println("No Posts")
				}
				/*if db.Where("blogger_id = ?", blogger.Id).Find(&posts).RecordNotFound() {
					fmt.Println("No Posts")
				}
				*/
				//db.Raw("select vd.id as visit_id , vis.id as visitor_id, CONCAT(vis.fname,' ',vis.lname) as visitor_name , vis.company as visitor_company , vis.phone as visitor_phone, vis.email as visitor_email, vis.profile_picture as image,  DATE_FORMAT(vd.in_time, '%h:%i %p') as in_time, vis.address as visitor_address ,  CONCAT(u.fname,' ',u.lname) as user_name , u.phone as user_phone from visit_details as vd, users as u, visitors as vis where u.id=vd.user_id and vis.id=vd.visitor_id and vd.in_time > ? and vd.location_id = ? and out_time is NULL", time.Now().In(Location).Format("2006-01-02"), locationID).Scan(&visitors)
				db.Raw("select * from post where blogger_id = ? order by id desc", blogger.Id).Scan(&posts)
				if len(posts) == 0 {
					has_posts = false
				}
			}else{				
				db.Raw("select * from post order by id desc").Scan(&posts)
				if db.Where("user_id = ?", user.Id).Find(&blogger).RecordNotFound() {
					fmt.Println("No Posts")
				}
			}
			NewNotifications,_ := custom.GetNotifications(db,blogger)
			//fmt.Println(NewNotifications)
			for _, postnotif := range NewNotifications {
				fmt.Println(postnotif)
			}
			notifCount := len(NewNotifications) 
			messages := H{
				"has_posts":   has_posts,
				"posts":       posts,
				"username":    user.Fname + " " + user.Lname,
                                "image":       blogger.Image,
				"project_url": GO_UKTRAV + "blog/add_article",
				"logout_url":  GO_UKTRAV + "blog/logout",
				"bid"       :  blogger.Id,
				"my_profile_url": GO_UKTRAV + "blog/profile/",
				"notifications" : NewNotifications,
				"notif_count": notifCount,
			}

			return c.Render(http.StatusOK, "dashboard.html", messages)
		}
		return sessionExpired(c)
	}
}

//blogger_profile
func blogger_profile(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		sess, _ := session.Get("session", c)
		if sess.Values["s_key"] == nil {
			return sessionExpired(c)
		}
		user, ok := custom.CheckSession(db, sess.Values["s_key"].(string))
		if ok {
			sess, _ := session.Get("session", c)
			sess.Options.MaxAge = 86400 * 7
			sess.Save(c.Request(), c.Response())

			id, _ := strconv.Atoi(c.Param("id"))
			var blogger models.Blogger
			if db.First(&blogger, id).RecordNotFound() {
				return c.Render(http.StatusNotFound, "not_found.html", H{})
			}

			if user.Id != blogger.UserId {
				return c.Render(http.StatusNotFound, "not_found.html", H{})
			}
			messages := H{
				"blogger":     blogger,
				"user"   :     user,
				"project_url": GO_UKTRAV + "blog/",
			}
			return c.Render(http.StatusOK, "my_profile.html", messages)
		}
		return sessionExpired(c)
	}
}


//profile_submit

func profile_submit(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		sess, _ := session.Get("session", c)
		if sess.Values["s_key"] == nil {
			return sessionExpired(c)
		}
		user, ok := custom.CheckSession(db, sess.Values["s_key"].(string))
		if ok {
			_ = user
			id, _ := strconv.Atoi(c.Param("id"))
			var blogger models.Blogger
			if db.First(&blogger, id).RecordNotFound() {
				return c.Render(http.StatusNotFound, "not_found.html", H{})
			}

			if user.Id != blogger.UserId {
				return c.Render(http.StatusNotFound, "not_found.html", H{})
			}

			shortBio := c.FormValue("short_bio")
			description := c.FormValue("description")
			formData, _ := c.MultipartForm()
			if blogger.ShortBio != shortBio {
				blogger.ShortBio = shortBio
			}
			if blogger.Description != description {
				blogger.Description = description
			}
			for _, fileHeader := range formData.File["file"] {
				if len(fileHeader.Filename) == 0 {
					continue
				}
				file, _ := fileHeader.Open()
				fileName := fileHeader.Filename
				path := "/home/arundeepak/Desktop/go/src/"
				//path := "/home/arundeepak92/webapps/uttarakhandtraveller_media_root/uploaded/"
				file_path := path + fileName
				url := "http://www.uttarakhandtraveller.com/media/uploaded/" + fileName
				f, err := os.OpenFile(file_path, os.O_WRONLY|os.O_CREATE, 0666)
				if err != nil {
					fmt.Println(err)
				}
				defer f.Close()
				io.Copy(f, file)
				blogger.Image = url
			}
			db.Save(&blogger)
			c.Redirect(http.StatusFound, "/blog/")
		}
		return sessionExpired(c)
	}
}


//edit_article
func edit_article(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		sess, _ := session.Get("session", c)
		if sess.Values["s_key"] == nil {
			return sessionExpired(c)
		}
		user, ok := custom.CheckSession(db, sess.Values["s_key"].(string))
		if ok {
			sess, _ := session.Get("session", c)
			sess.Options.MaxAge = 86400 * 7
			sess.Save(c.Request(), c.Response())

			id, _ := strconv.Atoi(c.Param("id"))
			var post models.Post
			if db.First(&post, id).RecordNotFound() {
				return c.Render(http.StatusNotFound, "not_found.html", H{})
			}
			var superuser models.Superuser
			if db.Where("user_id= ?", user.Id).First(&superuser).RecordNotFound(){
				var blogger models.Blogger
				if db.First(&blogger, post.BloggerId).RecordNotFound() {
					fmt.Println("No Blogger")
				}
				fmt.Println(user)
				if user.Id != blogger.UserId {
					return c.Render(http.StatusNotFound, "not_found.html", H{})
				}
			}
			images := []models.Image{}
			db.Raw("SELECT i.* from image as i,post_image as pi, post as p where pi.post_id=p.id and pi.image_id=i.id and pi.post_id = ?", post.Id).Scan(&images)
			messages := H{
				"title":       post.Title,
				"image":       "http://www.uttarakhandtraveller.com/media/blog/None/Kartik.jpg",
				"description": post.Description,
				"post_id":     post.Id,
				"images":      images,
				"project_url": GO_UKTRAV + "blog/",
			}
			return c.Render(http.StatusOK, "edit_article.html", messages)
		}

		return sessionExpired(c)
	}
}


//add_article
func add_article(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		sess, _ := session.Get("session", c)
		if sess.Values["s_key"] == nil {
			return sessionExpired(c)
		}
		_, ok := custom.CheckSession(db, sess.Values["s_key"].(string))
		if ok {
			sess, _ := session.Get("session", c)
			sess.Options.MaxAge = 86400 * 7
			sess.Save(c.Request(), c.Response())
			types := []models.PostType{} 
			db.Find(&types)
			messages := H{
				"project_url": GO_UKTRAV + "blog/",
				"submitted_url": GO_UKTRAV + "blog/articlesubmitted",
				"types" : types,
				"authtoken" : sess.Values["s_key"].(string),
			}
			return c.Render(http.StatusOK, "write_article.html", messages)
		}
		return sessionExpired(c)
	}
}

//article_submit

func article_submit(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id, _ := strconv.Atoi(c.QueryParam("id"))
		sess, _ := session.Get("session", c)
		if sess.Values["s_key"] == nil {
			return sessionExpired(c)
		}
		user, ok := custom.CheckSession(db, sess.Values["s_key"].(string))
		if ok {
			if id == 0 {
				fmt.Println("New Article")
				_ = user
				title := c.FormValue("title")
				description := c.FormValue("description")
				formData, _ := c.MultipartForm()
				var blogger models.Blogger
				db.Where("user_id=?", user.Id).First(&blogger)
				slug := custom.SlugMaker(title)
				var postType models.PostType
				types, _ := c.FormParams()		
				type_id := 4
				for post_type , _ := range(types){
					if !db.Where("type= ?", post_type).First(&postType).RecordNotFound(){
						type_id = postType.Id 
						break
					}					
				}
				post := models.Post{BloggerId: blogger.Id, Title: title, Description: description, CrDate: time.Now().In(Location), Status: 0, Slug: slug, TypeId: type_id}
				db.Create(&post)
				if db.NewRecord(post) {
					fmt.Println("Unable to create image")
				} 
				err := custom.UploadImage(db,post,formData)
				if err {
					fmt.Println("Upload successful")
				} 
			} else { // Existing Article
				fmt.Println("existing")
				var post models.Post
				db.First(&post, id)
				changed := false
				title := c.FormValue("title")
				if post.Title != title {
					post.Title = title
					slug := custom.SlugMaker(title)
					post.Slug = slug
					changed = true
				}
				description := c.FormValue("description")
				if post.Description != description {
					post.Description = description
					changed = true
				}
				formData, _ := c.MultipartForm()
				err := custom.UploadImage(db,post,formData)
				if !err {
					fmt.Println("Upload successful")
					changed = true
				}
				if changed {
					post.Status = 0
					db.Save(&post)
				}
				
			}
			c.Redirect(http.StatusFound, "/blog/")
		}
		return sessionExpired(c)
	}
}

//articleSubmittedPage
func articleSubmittedPage(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		messages := H{
			"project_url": GO_UKTRAV + "blog/",
		}
		return c.Render(http.StatusOK, "submitted.html", messages)
	}
}


func blogger_logout(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		return sessionExpired(c)
	}
}

//reset_password

func reset_password(db *gorm.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.Render(http.StatusNotFound, "not_found.html", H{})
	}
}




//###############################################################################################################

func sessionExpired(c echo.Context) error {
	sess, _ := session.Get("session", c)
	sess.Options.MaxAge = -1
	sess.Save(c.Request(), c.Response())
	messages := H{
		"message":      "Session Expired/ Logged out , Please login to continue",
		"register_url": GO_UKTRAV + "register",
		"project_url":  GO_UKTRAV + "reset_password",
	}
	return c.Render(http.StatusOK, "login.html", messages)
}

/*
func customHTTPErrorHandler(err error, c echo.Context) {
	code := http.StatusInternalServerError
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
	}
	errorPage := fmt.Sprintf("%d.html", code)
	if err := c.File(errorPage); err != nil {
		c.Logger().Error(err)
	}
	c.Logger().Error(err)
}

*/
